"use strict";

module.exports = app => {
  app._mongoDB = require("./services/mongoDB");
  app._Auth = require("./services/authentication")(app).expose;
  require("./controllers/main")(app);
};
