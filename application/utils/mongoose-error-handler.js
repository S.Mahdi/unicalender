"use strict";

module.exports = (err) => {
    let response = {};
    if (!err) {
        return err;
    } else if (err.errors) {
        Object.keys(err.errors).forEach((item) => {
            response[item] = err.errors[item].message;
        });
        return response;
    } else {
        return err.toString();
    }
};
