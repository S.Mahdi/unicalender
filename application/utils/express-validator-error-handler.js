"use strict";

/**
 * handling validation error responses ...
 * @param res
 * @param errorDetails
 * @returns {*|void}
 */
module.exports = function errorHandler(res, errorDetails) {

    console.log('VALIDATION_ERROR => Field Validation Filed => ' + JSON.stringify(errorDetails));

    errorDetails = errorDetails.map(function(item) {
        item.name = item.param;
        delete item.param;
        delete item.value;
        return item
    });

    let error = {
        errorCode: 'VALIDATION-ERROR',
        fields: errorDetails
    };
    return res.status(400).send(error);
};