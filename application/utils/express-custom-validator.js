"use strict";
module.exports = function() {
    return {
        isArray: function(value) {
            return Array.isArray(value);
        },
        isString: function(value) {
            return typeof value == 'string'
        },
        gte: function(param, num) {
            return param >= num;
        },
        isDate: function(value) {
            return !!new Date(value).getTime();
        }
    }
};
