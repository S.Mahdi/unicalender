"use strict";
module.exports = {
    isEmail: {
        validator: function(v) {
            return /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}$/.test(v);
        },
        message: '{VALUE} is not a valid Email address!'
    }
};
