"use strict";
const logger = require('../services/logger');
const errorHandlerFn = require('../utils/mongoose-error-handler');


class Members {

    constructor() {

    }

    toString() {
        return this.name.toString();
    }

    setModel(model) {
        this.model = model;
    }

    createMember(memberObject) {
        let newMember = new this.model(memberObject);

        return new Promise((resolve, reject) => {
            newMember.save((err, newRegisteredDocument) => {
                if (err) {
                    let error = errorHandlerFn(err);
                    if (err.code === 11000) {
                        logger.info('DB=>MemberModel=>createMember : ' + error);
                        return reject({
                            responseCode: 400,
                            errorObj: {
                                errorCode: 'UEXISTS',
                                additionalInformation: {
                                    message: 'You have already joined this class.'
                                }
                            }
                        });
                    } else {
                        logger.error('DB=>MemberModel=>createMember : ', error);
                        return reject({
                            responseCode: 500,
                            errorObj: {
                                errorCode: 'DBERROR',
                                additionalInformation: {
                                    message: error
                                }
                            }
                        });
                    }
                } else {
                    logger.success('DB=>MemberModel=>createMember: new Member added ', newRegisteredDocument);
                    return resolve(newRegisteredDocument);
                }
            });
        });
    }

    findByParams(query, projection) {
        return new Promise((resolve, reject) => {

            this.model.findOne(query, projection || {}, (err, response) => {
                let error = errorHandlerFn(err);
                if (err) {
                    logger.error('DB=>MemberModel=>findByParams : ' , error);
                    return reject({
                        responseCode: 500,
                        errorObj: {
                            errorCode: 'DBERROR',
                            additionalInformation: {
                                error: error
                            }
                        }
                    });
                } else if (!response) {
                    return reject({
                        responseCode: 404,
                        errorObj: {
                            errorCode: 'NOTFOUND',
                            additionalInformation: {
                                message: 'record does not exists'
                            }
                        }
                    });
                } else {
                    logger.log('DB=>MemberModel=>findByParams : ' , JSON.stringify(response));
                    return resolve(response);
                }
            });
        });
    }

    findAllByParams(query, projection) {
        return new Promise((resolve, reject) => {

            this.model.find(query, projection || {}, (err, response) => {
                let error = errorHandlerFn(err);
                if (err) {
                    logger.error('DB=>MemberModel=>findAllByParams : ' , error);
                    return reject({
                        responseCode: 500,
                        errorObj: {
                            errorCode: 'DBERROR',
                            additionalInformation: {
                                error: error
                            }
                        }
                    });
                } else if (!response) {
                    return reject({
                        responseCode: 404,
                        errorObj: {
                            errorCode: 'NOTFOUND',
                            additionalInformation: {
                                message: 'record does not exists'
                            }
                        }
                    });
                } else {
                    logger.log('DB=>MemberModel=>findAllByParams : ' , JSON.stringify(response));
                    return resolve(response);
                }
            });
        });
    }

    findAllByAggreagate(query, projection) {
        return new Promise((resolve, reject) => {

            this.model.aggregate([
                { $match: { ...query } },
                {
                  $lookup:
                    {
                      from: "classes",
                      localField: "classId",
                      foreignField: "_id",
                      as: "class"
                    }
               },
               { $project : projection || { class : 1 , _id: 0} }
             ] ,  (err, response) => {
                let error = errorHandlerFn(err);
                if (err) {
                    logger.error('DB=>MemberModel=>findAllByParams : ' , error);
                    return reject({
                        responseCode: 500,
                        errorObj: {
                            errorCode: 'DBERROR',
                            additionalInformation: {
                                error: error
                            }
                        }
                    });
                } else if (!response) {
                    return reject({
                        responseCode: 404,
                        errorObj: {
                            errorCode: 'NOTFOUND',
                            additionalInformation: {
                                message: 'record does not exists'
                            }
                        }
                    });
                } else {
                    logger.log('DB=>MemberModel=>findAllByParams : ' , JSON.stringify(response));
                    return resolve(response);
                }
            });
        });
    }

    updateByModel(model, properties) {
        return new Promise((resolve, reject) => {
            Object.assign(model, properties);
            model.save((err, updatedInfo) => {
                let error = errorHandlerFn(err);
                if(err){
                    if (err.code === 11000) {
                        logger.info('DB=>MemberModel=>updateByModel : ' + error);
                        return reject({
                            responseCode: 400,
                            errorObj: {
                                errorCode: 'UEXISTS',
                                additionalInformation: {
                                    message: 'This name has already been registered'
                                }
                            }
                        });
                    }else {
                        logger.error('DB=>MemberModel=>updateByModel: ' , error);
                        return reject({
                            responseCode: 500,
                            errorObj: {
                                errorCode: 'DBERROR',
                                additionalInformation: {
                                    error: error
                                }
                            }
                        });
                    }
                }
                logger.success('DB=>MemberModel=>updateByModel: ', updatedInfo);
                return resolve(updatedInfo);
            });
        });
    }

    deleteMember(filter) {
        return new Promise((resolve, reject) => {
            this.model.deleteOne(filter, (err, response) => {
                let error = errorHandlerFn(err);
                if (err) {
                    logger.error('DB=>MemberModel=>deleteMember : ' , error);
                    return reject({
                        responseCode: 500,
                        errorObj: {
                            errorCode: 'DBERROR',
                            additionalInformation: {
                                error: error
                            }
                        }
                    });
                } else if (response.n === 0) {
                    return reject({
                        responseCode: 404,
                        errorObj: {
                            errorCode: 'NOTFOUND',
                            additionalInformation: {
                                message: 'record does not exists'
                            }
                        }
                    });
                } else {
                    logger.log('DB=>MemberModel=>deleteMember : ' , JSON.stringify(response));
                    return resolve(response.n);
                }
            });
        });
    }
    
}

module.exports = new Members();
