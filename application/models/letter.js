"use strict";
const logger = require('../services/logger');
const errorHandlerFn = require('../utils/mongoose-error-handler');


class Letter {

    constructor() {

    }

    toString() {
        return this.name.toString();
    }

    setModel(model) {
        this.model = model;
    }

    createLetter(letterObject) {
        let newMember = new this.model(letterObject);

        return new Promise((resolve, reject) => {
            newMember.save((err, newRegisteredDocument) => {
                if (err) {
                    let error = errorHandlerFn(err);
                    if (err.code === 11000) {
                        logger.info('DB=>LetterModel=>createLetter : ' + error);
                        return reject({
                            responseCode: 400,
                            errorObj: {
                                errorCode: 'UEXISTS',
                                additionalInformation: {
                                    message: 'You have already joined this class.'
                                }
                            }
                        });
                    } else {
                        logger.error('DB=>LetterModel=>createLetter : ', error);
                        return reject({
                            responseCode: 500,
                            errorObj: {
                                errorCode: 'DBERROR',
                                additionalInformation: {
                                    message: error
                                }
                            }
                        });
                    }
                } else {
                    logger.success('DB=>LetterModel=>createLetter: new Leter added ', newRegisteredDocument);
                    return resolve(newRegisteredDocument);
                }
            });
        });
    }

    findByParams(query, projection) {
        return new Promise((resolve, reject) => {

            this.model.findOne(query, projection || {}, (err, response) => {
                let error = errorHandlerFn(err);
                if (err) {
                    logger.error('DB=>LetterModel=>findByParams : ' , error);
                    return reject({
                        responseCode: 500,
                        errorObj: {
                            errorCode: 'DBERROR',
                            additionalInformation: {
                                error: error
                            }
                        }
                    });
                } else if (!response) {
                    return reject({
                        responseCode: 404,
                        errorObj: {
                            errorCode: 'NOTFOUND',
                            additionalInformation: {
                                message: 'record does not exists'
                            }
                        }
                    });
                } else {
                    logger.log('DB=>LetterModel=>findByParams : ' , JSON.stringify(response));
                    return resolve(response);
                }
            });
        });
    }

    findAllByParams(query, projection) {
        return new Promise((resolve, reject) => {

            this.model.find(query, projection || {}, (err, response) => {
                let error = errorHandlerFn(err);
                if (err) {
                    logger.error('DB=>LetterModel=>findAllByParams : ' , error);
                    return reject({
                        responseCode: 500,
                        errorObj: {
                            errorCode: 'DBERROR',
                            additionalInformation: {
                                error: error
                            }
                        }
                    });
                } else if (!response) {
                    return reject({
                        responseCode: 404,
                        errorObj: {
                            errorCode: 'NOTFOUND',
                            additionalInformation: {
                                message: 'record does not exists'
                            }
                        }
                    });
                } else {
                    logger.log('DB=>LetterModel=>findAllByParams : ' , JSON.stringify(response));
                    return resolve(response);
                }
            });
        });
    }

    updateByModel(model, properties) {
        return new Promise((resolve, reject) => {
            Object.assign(model, properties);
            model.save((err, updatedInfo) => {
                let error = errorHandlerFn(err);
                if(err){
                    if (err.code === 11000) {
                        logger.info('DB=>LetterModel=>updateByModel : ' + error);
                        return reject({
                            responseCode: 400,
                            errorObj: {
                                errorCode: 'UEXISTS',
                                additionalInformation: {
                                    message: 'This name has already been registered'
                                }
                            }
                        });
                    }else {
                        logger.error('DB=>LetterModel=>updateByModel: ' , error);
                        return reject({
                            responseCode: 500,
                            errorObj: {
                                errorCode: 'DBERROR',
                                additionalInformation: {
                                    error: error
                                }
                            }
                        });
                    }
                }
                logger.success('DB=>LetterModel=>updateByModel: ', updatedInfo);
                return resolve(updatedInfo);
            });
        });
    }

    deleteLetter(filter) {
        return new Promise((resolve, reject) => {
            this.model.deleteOne(filter, (err, response) => {
                let error = errorHandlerFn(err);
                if (err) {
                    logger.error('DB=>LetterModel=>deleteLetter : ' , error);
                    return reject({
                        responseCode: 500,
                        errorObj: {
                            errorCode: 'DBERROR',
                            additionalInformation: {
                                error: error
                            }
                        }
                    });
                } else if (response.n === 0) {
                    return reject({
                        responseCode: 404,
                        errorObj: {
                            errorCode: 'NOTFOUND',
                            additionalInformation: {
                                message: 'record does not exists'
                            }
                        }
                    });
                } else {
                    logger.log('DB=>LetterModel=>deleteLetter : ' , JSON.stringify(response));
                    return resolve(response.n);
                }
            });
        });
    }
    
}

module.exports = new Letter();
