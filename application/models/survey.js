"use strict";
const logger = require('../services/logger');
const errorHandlerFn = require('../utils/mongoose-error-handler');


class Survey {

    constructor() {

    }

    toString() {
        return this.name.toString();
    }

    setModel(model) {
        this.model = model;
    }

    createSurvey(classObject) {
        let newClass = new this.model(classObject);

        return new Promise((resolve, reject) => {
            newClass.save((err, newRegisteredDocument) => {
                if (err) {
                    let error = errorHandlerFn(err);
                    console.log(err);
                    if (err.code === 11000) {
                        logger.info('DB=>SurveyModel=>createSurvey : ' + error);
                        return reject({
                            responseCode: 400,
                            errorObj: {
                                errorCode: 'UEXISTS',
                                additionalInformation: {
                                    message: 'duplicate key error'
                                }
                            }
                        });
                    } else {
                        logger.error('DB=>SurveyModel=>createSurvey : ', error);
                        return reject({
                            responseCode: 500,
                            errorObj: {
                                errorCode: 'DBERROR',
                                additionalInformation: {
                                    message: error
                                }
                            }
                        });
                    }
                } else {
                    logger.success('DB=>SurveyModel=>createSurvey: new Survey added ', newRegisteredDocument);
                    return resolve(newRegisteredDocument);
                }
            });
        });
    }

    findByParams(query, projection) {
        return new Promise((resolve, reject) => {

            this.model.findOne(query, projection || {}, (err, response) => {
                let error = errorHandlerFn(err);
                if (err) {
                    logger.error('DB=>SurveyModel=>findByParams : ' , error);
                    return reject({
                        responseCode: 500,
                        errorObj: {
                            errorCode: 'DBERROR',
                            additionalInformation: {
                                error: error
                            }
                        }
                    });
                } else if (!response) {
                    return reject({
                        responseCode: 404,
                        errorObj: {
                            errorCode: 'NOTFOUND',
                            additionalInformation: {
                                message: 'record does not exists'
                            }
                        }
                    });
                } else {
                    logger.log('DB=>SurveyModel=>findByParams : ' , JSON.stringify(response));
                    return resolve(response);
                }
            });
        });
    }

    
    findAllByParams(query, projection) {
        return new Promise((resolve, reject) => {

            this.model.find(query, projection || {}, (err, response) => {
                let error = errorHandlerFn(err);
                if (err) {
                    logger.error('DB=>SurveyModel=>findAllByParams : ' , error);
                    return reject({
                        responseCode: 500,
                        errorObj: {
                            errorCode: 'DBERROR',
                            additionalInformation: {
                                error: error
                            }
                        }
                    });
                } else if (!response) {
                    return reject({
                        responseCode: 404,
                        errorObj: {
                            errorCode: 'NOTFOUND',
                            additionalInformation: {
                                message: 'record does not exists'
                            }
                        }
                    });
                } else {
                    logger.log('DB=>SurveyModel=>findAllByParams : ' , JSON.stringify(response));
                    return resolve(response);
                }
            });
        });
    }

    updateByModel(model, properties) {
        return new Promise((resolve, reject) => {
            Object.assign(model, properties);
            model.save((err, updatedInfo) => {
                let error = errorHandlerFn(err);
                if(err){
                    if (err.code === 11000) {
                        logger.info('DB=>SurveyModel=>updateByModel : ' + error);
                        return reject({
                            responseCode: 400,
                            errorObj: {
                                errorCode: 'UEXISTS',
                                additionalInformation: {
                                    message: 'This name has already been registered'
                                }
                            }
                        });
                    }else {
                        logger.error('DB=>SurveyModel=>updateByModel: ' , error);
                        return reject({
                            responseCode: 500,
                            errorObj: {
                                errorCode: 'DBERROR',
                                additionalInformation: {
                                    error: error
                                }
                            }
                        });
                    }
                }
                logger.success('DB=>SurveyModel=>updateByModel: ', updatedInfo);
                return resolve(updatedInfo);
            });
        });
    }

    deleteSurvey(filter) {
        return new Promise((resolve, reject) => {
            this.model.deleteOne(filter, (err, response) => {
                let error = errorHandlerFn(err);
                if (err) {
                    logger.error('DB=>SurveyModel=>deleteSurvey : ' , error);
                    return reject({
                        responseCode: 500,
                        errorObj: {
                            errorCode: 'DBERROR',
                            additionalInformation: {
                                error: error
                            }
                        }
                    });
                } else if (response.n === 0) {
                    return reject({
                        responseCode: 404,
                        errorObj: {
                            errorCode: 'NOTFOUND',
                            additionalInformation: {
                                message: 'record does not exists'
                            }
                        }
                    });
                } else {
                    logger.log('DB=>SurveyModel=>deleteSurvey : ' , JSON.stringify(response));
                    return resolve(response.n);
                }
            });
        });
    }
    
}

module.exports = new Survey();
