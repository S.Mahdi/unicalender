"use strict";
const logger = require('../services/logger');
const errorHandlerFn = require('../utils/mongoose-error-handler');


class User {

    constructor() {

    }

    toString() {
        return this.name.toString();
    }

    setModel(model) {
        this.model = model;
    }

    createUser(userObject) {
        let newUser = new this.model(userObject);

        return new Promise((resolve, reject) => {
            newUser.save((err, newRegisteredDocument) => {
                if (err) {
                    let error = errorHandlerFn(err);
                    if (err.code === 11000) {
                        logger.info('DB=>UserModel=>createUser : ' + error);
                        return reject({
                            responseCode: 400,
                            errorObj: {
                                errorCode: 'UEXISTS',
                                additionalInformation: {
                                    message: 'This email has already been registered'
                                }
                            }
                        });
                    } else {
                        logger.error('DB=>UserModel=>createUser : ', error);
                        return reject({
                            responseCode: 500,
                            errorObj: {
                                errorCode: 'DBERROR',
                                additionalInformation: {
                                    message: error
                                }
                            }
                        });
                    }
                } else {
                    logger.success('DB=>UserModel=>createUser: new User registered ', newRegisteredDocument);
                    return resolve(newRegisteredDocument);
                }
            });
        });
    }

    findByParams(query, projection) {
        return new Promise((resolve, reject) => {

            this.model.findOne(query, projection || {}, (err, response) => {
                let error = errorHandlerFn(err);
                if (err) {
                    logger.error('DB=>UserModel=>findByParams : ' , error);
                    return reject({
                        responseCode: 500,
                        errorObj: {
                            errorCode: 'DBERROR',
                            additionalInformation: {
                                error: error
                            }
                        }
                    });
                } else if (!response) {
                    return reject({
                        responseCode: 404,
                        errorObj: {
                            errorCode: 'NOTFOUND',
                            additionalInformation: {
                                message: 'record does not exists'
                            }
                        }
                    });
                } else {
                    logger.log('DB=>UserModel=>findByParams : ' , JSON.stringify(response));
                    return resolve(response);
                }
            });
        });
    }

    updateByModel(model, properties) {
        return new Promise((resolve, reject) => {
            Object.assign(model, properties);
            model.save((err, updatedInfo) => {
                let error = errorHandlerFn(err);
                if(err){
                    if (err.code === 11000) {
                        logger.info('DB=>UserModel=>updateByModel : ' + error);
                        return reject({
                            responseCode: 400,
                            errorObj: {
                                errorCode: 'UEXISTS',
                                additionalInformation: {
                                    message: 'This name has already been registered'
                                }
                            }
                        });
                    }else {
                        logger.error('DB=>UserModel=>updateByModel: ' , error);
                        return reject({
                            responseCode: 500,
                            errorObj: {
                                errorCode: 'DBERROR',
                                additionalInformation: {
                                    error: error
                                }
                            }
                        });
                    }
                }
                logger.success('DB=>UserModel=>updateByModel: ', updatedInfo);
                return resolve(updatedInfo);
            });
        });
    }

    deleteUser(filter) {
        return new Promise((resolve, reject) => {
            this.model.deleteOne(filter, (err, response) => {
                let error = errorHandlerFn(err);
                if (err) {
                    logger.error('DB=>UserModel=>deleteUser : ' , error);
                    return reject({
                        responseCode: 500,
                        errorObj: {
                            errorCode: 'DBERROR',
                            additionalInformation: {
                                error: error
                            }
                        }
                    });
                } else if (response.n === 0) {
                    return reject({
                        responseCode: 404,
                        errorObj: {
                            errorCode: 'NOTFOUND',
                            additionalInformation: {
                                message: 'record does not exists'
                            }
                        }
                    });
                } else {
                    logger.log('DB=>UserModel=>deleteUser : ' , JSON.stringify(response));
                    return resolve(response.n);
                }
            });
        });
    }
    
}

module.exports = new User();
