"use strict";
const logger = require('../services/logger');
const errorHandlerFn = require('../utils/mongoose-error-handler');


class Class {

    constructor() {

    }

    toString() {
        return this.name.toString();
    }

    setModel(model) {
        this.model = model;
    }

    createClass(classObject) {
        let newClass = new this.model(classObject);

        return new Promise((resolve, reject) => {
            newClass.save((err, newRegisteredDocument) => {
                if (err) {
                    let error = errorHandlerFn(err);
                    console.log(err);
                    if (err.code === 11000) {
                        logger.info('DB=>ClassModel=>createClass : ' + error);
                        return reject({
                            responseCode: 400,
                            errorObj: {
                                errorCode: 'UEXISTS',
                                additionalInformation: {
                                    message: 'duplicate key error'
                                }
                            }
                        });
                    } else {
                        logger.error('DB=>ClassModel=>createClass : ', error);
                        return reject({
                            responseCode: 500,
                            errorObj: {
                                errorCode: 'DBERROR',
                                additionalInformation: {
                                    message: error
                                }
                            }
                        });
                    }
                } else {
                    logger.success('DB=>ClassModel=>createClass: new Class added ', newRegisteredDocument);
                    return resolve(newRegisteredDocument);
                }
            });
        });
    }

    findByParams(query, projection) {
        return new Promise((resolve, reject) => {

            this.model.findOne(query, projection || {}, (err, response) => {
                let error = errorHandlerFn(err);
                if (err) {
                    logger.error('DB=>ClassModel=>findByParams : ', error);
                    return reject({
                        responseCode: 500,
                        errorObj: {
                            errorCode: 'DBERROR',
                            additionalInformation: {
                                error: error
                            }
                        }
                    });
                } else if (!response) {
                    return reject({
                        responseCode: 404,
                        errorObj: {
                            errorCode: 'NOTFOUND',
                            additionalInformation: {
                                message: 'record does not exists'
                            }
                        }
                    });
                } else {
                    logger.log('DB=>ClassModel=>findByParams : ', JSON.stringify(response));
                    return resolve(response);
                }
            });
        });
    }

    findAllByAggreagate(query, projection) {
        return new Promise((resolve, reject) => {

            this.model.aggregate([{
                    $match: {
                        ...query
                    }
                },
                {
                    $lookup: {
                        from: "users",
                        localField: "createrId",
                        foreignField: "_id",
                        as: "teacher"
                    },
                    
                },
                {
                    $project: {
                        privateTask: 0,
                        "teacher.todo": 0,
                        "teacher.privateTask": 0,
                        "teacher.password": 0,
                        "teacher.verifyCode": 0,
                        "teacher.createdAt": 0,
                        "teacher.updatedAt": 0,
                        "teacher.updatedAt": 0,
                        "teacher._id": 0,
                        createdAt: 0,
                        updatedAt: 0,
                    }
                }
            ], (err, response) => {
                let error = errorHandlerFn(err);
                if (err) {
                    logger.error('DB=>MemberModel=>findAllByParams : ', error);
                    return reject({
                        responseCode: 500,
                        errorObj: {
                            errorCode: 'DBERROR',
                            additionalInformation: {
                                error: error
                            }
                        }
                    });
                } else if (!response) {
                    return reject({
                        responseCode: 404,
                        errorObj: {
                            errorCode: 'NOTFOUND',
                            additionalInformation: {
                                message: 'record does not exists'
                            }
                        }
                    });
                } else {
                    logger.log('DB=>MemberModel=>findAllByParams : ', JSON.stringify(response));
                    return resolve(response);
                }
            });
        });
    }

    findAllByParams(query, projection) {
        return new Promise((resolve, reject) => {

            this.model.find(query, projection || {}, (err, response) => {
                let error = errorHandlerFn(err);
                if (err) {
                    logger.error('DB=>ClassModel=>findAllByParams : ', error);
                    return reject({
                        responseCode: 500,
                        errorObj: {
                            errorCode: 'DBERROR',
                            additionalInformation: {
                                error: error
                            }
                        }
                    });
                } else if (!response) {
                    return reject({
                        responseCode: 404,
                        errorObj: {
                            errorCode: 'NOTFOUND',
                            additionalInformation: {
                                message: 'record does not exists'
                            }
                        }
                    });
                } else {
                    logger.log('DB=>ClassModel=>findAllByParams : ', JSON.stringify(response));
                    return resolve(response);
                }
            });
        });
    }

    updateByModel(model, properties) {
        return new Promise((resolve, reject) => {
            Object.assign(model, properties);
            model.save((err, updatedInfo) => {
                let error = errorHandlerFn(err);
                if (err) {
                    if (err.code === 11000) {
                        logger.info('DB=>ClassModel=>updateByModel : ' + error);
                        return reject({
                            responseCode: 400,
                            errorObj: {
                                errorCode: 'UEXISTS',
                                additionalInformation: {
                                    message: 'This name has already been registered'
                                }
                            }
                        });
                    } else {
                        logger.error('DB=>ClassModel=>updateByModel: ', error);
                        return reject({
                            responseCode: 500,
                            errorObj: {
                                errorCode: 'DBERROR',
                                additionalInformation: {
                                    error: error
                                }
                            }
                        });
                    }
                }
                logger.success('DB=>ClassModel=>updateByModel: ', updatedInfo);
                return resolve(updatedInfo);
            });
        });
    }

    deleteClass(filter) {
        return new Promise((resolve, reject) => {
            this.model.deleteOne(filter, (err, response) => {
                let error = errorHandlerFn(err);
                if (err) {
                    logger.error('DB=>ClassModel=>deleteClass : ', error);
                    return reject({
                        responseCode: 500,
                        errorObj: {
                            errorCode: 'DBERROR',
                            additionalInformation: {
                                error: error
                            }
                        }
                    });
                } else if (response.n === 0) {
                    return reject({
                        responseCode: 404,
                        errorObj: {
                            errorCode: 'NOTFOUND',
                            additionalInformation: {
                                message: 'record does not exists'
                            }
                        }
                    });
                } else {
                    logger.log('DB=>ClassModel=>deleteClass : ', JSON.stringify(response));
                    return resolve(response.n);
                }
            });
        });
    }

}

module.exports = new Class();