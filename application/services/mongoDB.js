"use strict";
const mongoose = require('mongoose');
const CONFIG = require('../config/' + process.env.NODE_ENV + '.js');



let connectionString = 'mongodb://' + CONFIG.mongoAddr + ':' + CONFIG.mongoPort + '/' + CONFIG.mongoDataBaseName;

if (process.env.NODE_ENV === 'production') {
    connectionString = "mongodb://e7fc003e6384d60832b047b452f76834:830330@9a.mongo.evennode.com:27017,9b.mongo.evennode.com:27017/e7fc003e6384d60832b047b452f76834?replicaSet=eu-9"
}

let connectionOptions = {
    keepAlive: 120,
    socketTimeoutMS: 30000,
    useNewUrlParser: true,
    useCreateIndex: true
};


if (CONFIG.mongoUser !== '' && CONFIG.mongoPass !== '') {

    // Object.assign(connectionOptions, {
    //     user: CONFIG.mongoUser,
    //     pass: CONFIG.mongoPass,
    //     auth : {
    //         authdb : "admin"
    //     }
    // });
    
}


mongoose.connect(connectionString , connectionOptions);

mongoose.connection.on('error', (err) => {
    console.log('ERROR_WHILE_CONNECTING_MONGO_DB: => ' + err.message);
});

mongoose.connection.once('open', () => {
    console.log('+++ MONGO_DB_CONNECTED_SUCCESSFULLY');
});

module.exports = {
    User: require('../schema/user')(mongoose),
    Member: require('../schema/member')(mongoose),
    Class: require('../schema/class')(mongoose),
    Letter: require('../schema/letter')(mongoose),
    Survey: require('../schema/survey')(mongoose),
};
