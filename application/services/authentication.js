"use strict"

const cryptoJS  = require('crypto-js');
const jwt = require('jsonwebtoken');
const userModel = require('../models/user');


class authentication {

    constructor(app){

        userModel.setModel(app._mongoDB.User);

        this.expose = {
            userAuth: this.userAuth,
            isUserLoggedIn: this.isUserLoggedIn,
            isTeacherLoggedIn: this.isTeacherLoggedIn
        };
    }


    async userAuth(req, res, next) {
        try {
            let pwd = cryptoJS.SHA256(req.body.password).toString();
            let userInfo = await userModel.findByParams({email: req.body.email, password: pwd});

            if( userInfo.verifyCode !== "Actived") {
                res.status(401).send({message: "the email hasn't verified"})
            }

            if(userInfo !== null) {
                userInfo.password = undefined;
                let token = await authentication.generateUserToken.apply(this, [{
                    userId: userInfo._id,
                    type: userInfo.type,
                    username: userInfo.username
                }]);
                res.set('Authorization', token);
                res._user = userInfo;
                next();
            }
        } catch (error) {
            this._catchErrorHandler(error, res);
        }
    }


    async isUserLoggedIn(req, res, next){
        let tokenKey = req.get('authorization');

        if( tokenKey === undefined) {
            res.status(401).send({message: 'unAuthorized'})
        }else{
            try {
                let tokenString = tokenKey.split(' ')[1];
                req._user = await authentication.verifyUserToken.apply(this, [tokenString, this._CONFIG.secretKey]);
                next();
            } catch(error) {
                return res.status(401).send({message: 'unAuthorized'});
            }
        }
    }

    async isTeacherLoggedIn(req, res, next){
        let tokenKey = req.get('authorization');

        if( tokenKey === undefined) {
            res.status(401).send({message: 'unAuthorized'})
        }else{
            try {
                let tokenString = tokenKey.split(' ')[1];
                
                req._user = await authentication.verifyUserToken.apply(this, [tokenString, this._CONFIG.secretKey]);

                if (req._user.type !== this._CONFIG.userTypes.Teacher) {
                    throw 'type incorrect user must be teacher'
                }

                next();
            } catch(error) {
                return res.status(401).send({message: 'unAuthorized'});
            }
        }
    }


    static generateUserToken(userInfo) {
        return new Promise((resolve, reject) => {
            jwt.sign(userInfo, this._CONFIG.secretKey, {expiresIn: '1d'},
            (error, token) => {
                if(error){
                    reject(error)
                }
                resolve(token);
            });
        });
    }

    static verifyUserToken(token, secureKey) {
        return new Promise((resolve, reject) => {
            jwt.verify(token, this._CONFIG.secretKey, function(error, decoded) {
                if(error){
                    reject(error);
                }
                resolve(decoded)
            });
        });
    }

}

module.exports = (app) => new authentication(app);