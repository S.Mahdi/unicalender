"use strict"

const router = require('express').Router();
const UserModel = require('../models/user');
const taskValidation = require('./validators/addTask');

class PrivateTask {

    constructor(app) {

        router.post('/Task', [app._Auth.isUserLoggedIn.bind(this), taskValidation.addTask], this.addTask.bind(this));
        router.get('/Task', [app._Auth.isUserLoggedIn.bind(this) ], this.getTask.bind(this));
        router.delete('/Task/:id', [app._Auth.isUserLoggedIn.bind(this)], this.deleteTask.bind(this));
        router.patch('/Task/:id', [app._Auth.isUserLoggedIn.bind(this)], this.editTask.bind(this));

        router.get('/allTask', [app._Auth.isUserLoggedIn.bind(this)], this.getAllTask.bind(this));
        router.delete('/allTask', [app._Auth.isUserLoggedIn.bind(this)], this.deleteAllTask.bind(this));

        UserModel.setModel(app._mongoDB.User);
        app.use('/user', router);
        Object.assign(this, app);
    }

    async addTask(req, res) {
        try {
            let userModel = await UserModel.findByParams({_id: req._user.userId});

            req.body.id = Date.now().toString() + "-" + (Math.floor(Math.random() * (999 - 0 + 1)) + 0);
            userModel.privateTask.push(req.body);

            let result = await UserModel.updateByModel(userModel);

            res.send({privateTask: result.privateTask});
        } catch (error) {
            this._catchErrorHandler(error, res)
        }
    }

    async deleteTask(req, res) {
        try {
            let userModel = await UserModel.findByParams({_id: req._user.userId});
            delete req.body.id;

            let beforDeleteLength = userModel.privateTask.length;
            userModel.privateTask = userModel.privateTask.filter(element => element.id !== req.params.id)            

            userModel.markModified('privateTask')
            let result = await UserModel.updateByModel(userModel);
            let afterDeleteLength = result.privateTask.length;


            let deleted = beforDeleteLength - afterDeleteLength
            res.send({privateTask: result.privateTask, deletedObjectNum: deleted});
        } catch (error) {
            this._catchErrorHandler(error, res)
        }
    }

    async getTask(req, res) {
        try {
            let userModel = await UserModel.findByParams({_id: req._user.userId});
            delete req.body.id;

            let filter = userModel.privateTask.filter(element => element.date === req.query.date || element.id === req.query.id )
            
            res.send({result: filter.length > 0 ? filter : 'notFound'});
        } catch (error) {
            this._catchErrorHandler(error, res)
        }
    }

    async editTask(req, res) {
        try {
            let userModel = await UserModel.findByParams({_id: req._user.userId});

            let changedNum = 0;
            userModel.privateTask = userModel.privateTask.map(element => {
                if(element.id === req.params.id) {
                    delete req.body.id;
                    changedNum = 1;
                    return Object.assign(element, req.body)
                }
                return element
            })

            userModel.markModified('privateTask')
            let result = await UserModel.updateByModel(userModel);

            res.send({privateTask: result.privateTask, changedObjectNum: changedNum});
        } catch (error) {
            this._catchErrorHandler(error, res)
        }
    }

    async getAllTask(req, res) {
        try {
            let projection = {
                privateTask: 1,
                _id: 0
            }
            let result = await UserModel.findByParams({_id: req._user.userId}, projection);            

            res.send({userInfo: result});
        } catch (error) {
            this._catchErrorHandler(error, res)
        }
    }

    async deleteAllTask(req, res) {
        try {
            let userModel = await UserModel.findByParams({_id: req._user.userId});

            let result = await UserModel.updateByModel(userModel, {privateTask: []});

            res.send({message: 'success'});
        } catch (error) {
            this._catchErrorHandler(error, res)
        }
    }

}

module.exports = (app) => new PrivateTask(app);