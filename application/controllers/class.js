"use strict"

const router = require('express').Router();
const ClassModel = require('../models/class');
const MemberModel = require('../models/members');
const LetterModel = require('../models/letter');
const UserModel = require('../models/user');
const classValidation = require('./validators/class');

class Class {

    constructor(app) {

        router.post('/class', [classValidation.addClass, app._Auth.isTeacherLoggedIn.bind(this)], this.createClass.bind(this));
        router.delete('/class/:classId', [app._Auth.isTeacherLoggedIn.bind(this)], this.deleteClass.bind(this));
        router.patch('/class/:classId', [app._Auth.isTeacherLoggedIn.bind(this)], this.updateClass.bind(this));
        router.get('/class', [app._Auth.isTeacherLoggedIn.bind(this)], this.myClasses.bind(this));


        router.post('/class/sendLettter/SendToAll/:classId', [classValidation.sendToAll, app._Auth.isTeacherLoggedIn.bind(this)], this.sendToAll.bind(this));
        router.post('/class/sendLettter/SendToUser/:userId', [classValidation.sendToUser, app._Auth.isTeacherLoggedIn.bind(this)], this.sendToUser.bind(this));
        router.get('/class/getAllLetters/:date', [app._Auth.isUserLoggedIn.bind(this)], this.getLetters.bind(this));

        router.post('/class/privateLink/:userEmail', [classValidation.sendPrivateLink, app._Auth.isTeacherLoggedIn.bind(this)], this.sendPrivateLink.bind(this));
        router.get('/class/privateLink/:letterId', [app._Auth.isUserLoggedIn.bind(this)], this.joinPrivateLink.bind(this));

        router.get('/class/public', [app._Auth.isUserLoggedIn.bind(this)], this.publicClassShow.bind(this));
        
        
        
        router.get('/photo/:id', this.getClassPhoto.bind(this));


        ClassModel.setModel(app._mongoDB.Class);
        LetterModel.setModel(app._mongoDB.Letter);
        MemberModel.setModel(app._mongoDB.Member);
        UserModel.setModel(app._mongoDB.User);

        app.use(router);
        Object.assign(this, app);
    }

    async createClass(req, res) {
        try {
            if (req.body.photoUrl !== undefined) {
                // let photo = req.body.photoUrl
                // let base64 = photo.replace(/^data:image\/png;base64,/, '')
                // let dirpath = 'storage/'
                // let imageName = Date.now() + "-" + (Math.floor(Math.random() * (99 - 0 + 1)) + 0) + '.jpg'
                // let imageLocation = dirpath + imageName
                // fs.writeFile(imageLocation, base64, 'base64', function (err) {
                //     if (err) throw err;
                // });
            }else {
                delete req.body.photoUrl
            }
            
            delete req.body.Survey;
            
            req.body.createrId = req._user.userId
            let newClass = await ClassModel.createClass( req.body );


            delete newClass._doc.createdAt;
            delete newClass._doc.updatedAt;

            
            res.send({ClassInfo: newClass})

        } catch (error) {
            this._catchErrorHandler(error, res);
        }
    }

    async getClassPhoto(req, res) {
        res.download(`storage/${req.params.id}`)
    }

    async deleteClass(req, res) {
        try {
            let result = await ClassModel.deleteClass({
                _id: req.params.classId,
                createrId: req._user.userId
            })
            
            res.send({result, message:'success'});
        } catch (error) {
            this._catchErrorHandler(error, res);
        }
    }


    async updateClass(req, res) {
        try {
            let classModel = await ClassModel.findByParams({createrId: req._user.userId, _id: req.params.classId});

            if (req.body.photoUrl !== undefined) {
                // let photo = req.body.photoUrl
                // let base64 = photo.replace(/^data:image\/png;base64,/, '')
                // let dirpath = 'storage/'
                // let imageName = Date.now() + "-" + (Math.floor(Math.random() * (99 - 0 + 1)) + 0) + '.jpg'
                // let imageLocation = dirpath + imageName
                // fs.writeFile(imageLocation, base64, 'base64', function (err) {
                //     if (err) throw err;
                // });
                // req.body.photoUrl = process.env.DOMAIN + '/class/photo/' + imageName
            }else {
                delete req.body.photoUrl
            }

            delete req.body.createrId;
            delete req.body._id;

            let result = await ClassModel.updateByModel(classModel, req.body);

            delete result._doc.updatedAt
            delete result._doc.createdAt

            res.send({result});
        } catch (error) {
            this._catchErrorHandler(error, res)
        }
    }

    async myClasses(req, res) {
        try {

            let classId = req.query.classId ? { _id: req.query.classId } : null

            let result = await ClassModel.findAllByParams({createrId: req._user.userId, ...classId},{
                createdAt: 0,
                updatedAt: 0
            });            

            res.send({teacherClasses: result});
        } catch (error) {
            this._catchErrorHandler(error, res)
        }
    }


    async publicClassShow(req, res) {
        try {
            let result = await ClassModel.findAllByAggreagate({accessLevel : true});            

            res.send({publicClasses: result});
        } catch (error) {
            this._catchErrorHandler(error, res)
        }
    }

    async sendToAll(req, res) {
        try{
            
            let result = await LetterModel.createLetter({
                content: req.body.content,
                title: req.body.title,
                classId: req.params.classId
            })

            res.send({message: 'success'});
        }catch (error) {
            this._catchErrorHandler(error, res)
        }
    }

    async sendToUser(req, res) {
        try{
            
            let result = await LetterModel.createLetter({
                content: req.body.content,
                title: req.body.title,
                userId: req.params.userId
            })

            res.send({result});
        }catch (error) {
            this._catchErrorHandler(error, res)
        }
    }

    async getLetters(req, res) {
        try{

            let result = await MemberModel.findAllByParams({userId: req._user.userId}, {classId: 1, _id: 0})

            result = result.map(element => element.classId);
            
            let finalResult = await LetterModel.findAllByParams({
                $or: [{userId: req._user.userId}, {classId: {$in: result }}],
                createdAt: { '$gte' : new Date(req.params.date)}
            })

            res.send({finalResult});
        }catch (error) {
            this._catchErrorHandler(error, res)
        }
    }

    async sendPrivateLink(req, res) {
        try{
            let user = await UserModel.findByParams({email: req.params.userEmail})            
            
            let result = await LetterModel.createLetter({
                content: req.body.content,
                title: req.body.title,
                userId: user._id,
                classId: req.body.classId
            })

            res.send({message: 'success'});
        }catch (error) {
            this._catchErrorHandler(error, res)
        }
    }

    async joinPrivateLink(req, res) {
        try{
            let [result] = await LetterModel.findAllByParams({ _id: req.params.letterId })
            let deleteInfo = await LetterModel.deleteLetter({ _id: req.params.letterId })

            let createMemeber = await MemberModel.createMember({
                classId: result.classId,
                userId: result.userId,
                username: result.username
            })

            res.send({message: 'success'});
        }catch (error) {
            this._catchErrorHandler(error, res)
        }
    }

}

module.exports = (app) => new Class(app);