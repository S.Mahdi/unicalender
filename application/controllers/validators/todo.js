"use strict";
module.exports = {
    addTodo: (req, res, next) => {
        let schema = {
            'title': {
                in: 'body',
                notEmpty: true,                
            }
        };

        req.check(schema);
        let errors = req.validationErrors();
        if (errors) {
            return new req._errorHandler(res, errors);
        }
        next();
    },
    // deleteTask: (req, res, next) => {
    //     let schema = {
    //         'date': {
    //             in: 'params',
    //             notEmpty: true,                
    //         }
    //     };

    //     req.check(schema);
    //     let errors = req.validationErrors();
    //     if (errors) {
    //         return new req._errorHandler(res, errors);
    //     }
    //     next();
    // },
    getTodo: (req, res, next) => {
        let schema = {
            'title': {
                in: 'query',
                notEmpty: true,                
            }
        };

        req.check(schema);
        let errors = req.validationErrors();
        if (errors) {
            return new req._errorHandler(res, errors);
        }
        next();
    },
};