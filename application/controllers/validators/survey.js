"use strict";
module.exports = {
    addSurvey: (req, res, next) => {
        let schema = {
            'title': {
                in: 'body',
                notEmpty: true,                
            },
            'description': {
                in: 'body',
                notEmpty: true,                
            },
            'options': {
                in: 'body',
                notEmpty: true,                
            },

        };

        req.check(schema);
        let errors = req.validationErrors();
        if (errors) {
            return new req._errorHandler(res, errors);
        }
        next();
    },
};