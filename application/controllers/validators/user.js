"use strict";
module.exports = {
    createUser: (req, res, next) => {
        let schema = {
            'username': {
                in: 'body',
                notEmpty: true,                
            },
            'password': {
                in: 'body',
                notEmpty: true,                
            },
            'email': {
                in: 'body',
                notEmpty: true,
                isEmail: true      
            },
        };

        req.check(schema);
        let errors = req.validationErrors();
        if (errors) {
            return new req._errorHandler(res, errors);
        }
        next();
    },

    login: (req, res, next) => {
        let schema = {        
            'password': {
                in: 'body',
                notEmpty: true,                
            },
            'email': {
                in: 'body',
                isEmail: true,
                notEmpty: true,                
            },
        };

        req.check(schema);
        let errors = req.validationErrors();
        if (errors) {
            return new req._errorHandler(res, errors);
        }
        next();
    }
};