"use strict";
module.exports = {
    addClass: (req, res, next) => {
        let schema = {
            'className': {
                in: 'body',
                notEmpty: true,                
            },
            'classEmail': {
                in: 'body',
                isEmail: true,
                notEmpty: true,                
            },
        };

        req.check(schema);
        let errors = req.validationErrors();
        if (errors) {
            return new req._errorHandler(res, errors);
        }
        next();
    },
    sendToAll: (req, res, next) => {
        let schema = {
            'content': {
                in: 'body',
                notEmpty: true,                
            },
            'title': {
                in: 'body',
                notEmpty: true,                
            },
        };

        req.check(schema);
        let errors = req.validationErrors();
        if (errors) {
            return new req._errorHandler(res, errors);
        }
        next();
    },
    sendToUser: (req, res, next) => {
        let schema = {
            'content': {
                in: 'body',
                notEmpty: true,                
            },
            'title': {
                in: 'body',
                notEmpty: true,                
            },
        };

        req.check(schema);
        let errors = req.validationErrors();
        if (errors) {
            return new req._errorHandler(res, errors);
        }
        next();
    },
    sendPrivateLink: (req, res, next) => {
        let schema = {
            'content': {
                in: 'body',
                notEmpty: true,                
            },
            'title': {
                in: 'body',
                notEmpty: true,                
            },
            'classId': {
                in: 'body',
                notEmpty: true,                
            },
            'userEmail': {
                in: 'params',
                notEmpty: true,     
                isEmail: true           
            },
        };

        req.check(schema);
        let errors = req.validationErrors();
        if (errors) {
            return new req._errorHandler(res, errors);
        }
        next();
    },
};