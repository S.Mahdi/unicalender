"use strict";
module.exports = {
    addTask: (req, res, next) => {
        let schema = {
            'date': {
                in: 'body',
                notEmpty: true,                
            }
        };

        req.check(schema);
        let errors = req.validationErrors();
        if (errors) {
            return new req._errorHandler(res, errors);
        }
        next();
    },
    // deleteTask: (req, res, next) => {
    //     let schema = {
    //         'date': {
    //             in: 'params',
    //             notEmpty: true,                
    //         }
    //     };

    //     req.check(schema);
    //     let errors = req.validationErrors();
    //     if (errors) {
    //         return new req._errorHandler(res, errors);
    //     }
    //     next();
    // },
    getTask: (req, res, next) => {
        let schema = {
            'date': {
                in: 'query',
                notEmpty: true,                
            }
        };

        req.check(schema);
        let errors = req.validationErrors();
        if (errors) {
            return new req._errorHandler(res, errors);
        }
        next();
    },
};