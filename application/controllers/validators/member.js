"use strict";
module.exports = {
    joinClass: (req, res, next) => {
        let schema = {
            'classId': {
                in: 'body',
                notEmpty: true,                
            },
        };

        req.check(schema);
        let errors = req.validationErrors();
        if (errors) {
            return new req._errorHandler(res, errors);
        }
        next();
    },
};