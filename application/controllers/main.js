"use strict"
module.exports = (app) => {

     /**
     * handling common errors in catch section
     * @param error
     * @param res
     * @private
     */
    app._catchErrorHandler = function(error, res) {
        let statusCode = error.responseCode || 500;
        let errorOutput = error.errorObj || {message: 'unknown Error'};
        if (statusCode === 500) {
            console.error('Catch Handler => ', error);
        } else {
            console.log('Catch Handler => ', error);
        }
        res.status(statusCode).send(errorOutput);
    };

    //require all controllers
    require('../controllers/user')(app);
    require('../controllers/privateTask')(app);
    require('../controllers/todo')(app);
    require('../controllers/class')(app);
    require('../controllers/member')(app);
    require('../controllers/survey')(app);
    
}