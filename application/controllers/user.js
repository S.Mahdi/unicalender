"use strict"
const express = require('express');
const router = require('express').Router();
const UserModel = require('../models/user');
const userValidation = require('./validators/user'); 
const cryptoJS  = require('crypto-js');
const mailer = require('../services/mailer');
const path = require('path')

class User {

    constructor(app) {

        router.post('/User', [userValidation.createUser], this.createUser.bind(this));
        router.delete('/User', [app._Auth.isUserLoggedIn.bind(this)], this.deleteUser.bind(this));
        router.patch('/User', [app._Auth.isUserLoggedIn.bind(this)], this.updateUser.bind(this));
        router.get('/User', [app._Auth.isUserLoggedIn.bind(this)], this.getUserInfo.bind(this));
        router.get('/User/verify/:id', [], this.verifyUser.bind(this));
        
        router.post('/User/login', [userValidation.login, app._Auth.userAuth.bind(this)], this.login);

        router.get('/User/loginPage', [], this.loginPage);
        
        router.get('/', (req, res) => { res.redirect('/User/loginPage')})
        app.use(express.static(path.join(__dirname, '../public')))

        UserModel.setModel(app._mongoDB.User);
        app.use(router);
        Object.assign(this, app);
    }

    async createUser(req, res) {
        try {
            delete req.body.type;
            delete req.body.userVerified;

            req.body.password = cryptoJS.SHA256(req.body.password).toString();
            req.body.verifyCode = this.createVerifyCode();

            req.body.email = req.body.email.toLowerCase();

            let newUser = await UserModel.createUser( req.body );

            mailer.SendEmail([req.body.email], "uniCalender", mailer.html(`${process.env.DOMAIN}/user/verify/${newUser._id}`),
            function (data) {
                if (data.status == "no") {
                    throw "cant send email to " + req.body.email
                } 
            });        

            res.sendFile('success.html', {
                root: path.join(__dirname, '../public')
            })

        } catch (error) {
            this._catchErrorHandler(error, res);
        }
    }

    createVerifyCode() {
        var token = Math.floor(Math.random() * (99999 - 10000 + 1)) + 10000;
        return token.toString();
    }

    async login(req, res) {
        res.send({message:"success"});
    }

    async deleteUser(req, res) {
        try {
            let result = await UserModel.deleteUser({
                _id: req._user.userId
            })

            res.send({result, message:'success'});
        } catch (error) {
            this._catchErrorHandler(error, res);
        }
    }

    async updateUser(req, res) {
        try {
            let userModel = await UserModel.findByParams({_id: req._user.userId});

            delete req.body.id;
            delete req.body.email;
            delete req.body.verifyCode;
            delete req.body.userVerified;
            

            if(req.body.type == 1 || (req.body.type != 2 && req.body.type != 3) ) {
                throw {
                    responseCode: 400,
                    errorObj: {
                        errorCode: 'ACCESS DENIED',
                        additionalInformation: {
                            message: "the type value is incorrect"
                        }
                    }
                }
            }

            "password" in  req.body ? req.body.password = cryptoJS.SHA256(req.body.password).toString() : null
            
            let result = await UserModel.updateByModel(userModel, req.body);

            delete result._doc.password;
            delete result._doc.createdAt;
            delete result._doc.updatedAt;

            res.send({result});
        } catch (error) {
            this._catchErrorHandler(error, res)
        }
    }

    async verifyUser(req, res) {
        try {
            let userModel = await UserModel.findByParams({_id: req.params.id});

            let result = await UserModel.updateByModel(userModel, {userVerified : true, verifyCode: "Actived"});

            res.send("actived :)");
        } catch (error) {
            this._catchErrorHandler(error, res)
        }
    }

    async getUserInfo(req, res) {
        try {
            let projection = {
                username: 1,
                email: 1,
                type: 1,
                privateTask: 1,
                todo: 1,
                userVerified: 1,
                _id: 0
            }
            let result = await UserModel.findByParams({_id: req._user.userId}, projection);            

            res.send({userInfo: result});
        } catch (error) {
            this._catchErrorHandler(error, res)
        }
    }

    async loginPage(req, res) {
        res.sendFile('index.html', {
            root: path.join(__dirname, '../public')
        })
    }
}

module.exports = (app) => new User(app);