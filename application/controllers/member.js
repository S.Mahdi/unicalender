"use strict"

const router = require('express').Router();
const ObjectId = require('mongoose').Types.ObjectId
const MemberModel = require('../models/members');
const memberValidation = require('./validators/member');

class Member {

    constructor(app) {

        router.post('/member', [memberValidation.joinClass, app._Auth.isUserLoggedIn.bind(this)], this.joinClass.bind(this));
        router.delete('/member/:classId', [app._Auth.isUserLoggedIn.bind(this)], this.leaveClass.bind(this));
        router.get('/member/class', [app._Auth.isUserLoggedIn.bind(this)], this.myClasses.bind(this));
        router.get('/member/class/tasks', [app._Auth.isUserLoggedIn.bind(this)], this.getTask.bind(this));
        router.get('/member/class/student/getAll/:classId', [app._Auth.isTeacherLoggedIn.bind(this)], this.student.bind(this));
        
        router.delete('/member/removeStudent/:stuId/:classId', [app._Auth.isTeacherLoggedIn.bind(this)], this.removeStudent.bind(this));
        


        MemberModel.setModel(app._mongoDB.Member);
        app.use(router);
        Object.assign(this, app);
    }

    async joinClass(req, res) {
        try {        
            let newMember = await MemberModel.createMember({
                classId: req.body.classId,
                userId: req._user.userId,
                username: req._user.username
            });

            delete newMember._doc.updatedAt
            delete newMember._doc._id
            
            res.send({member: newMember})

        } catch (error) {
            this._catchErrorHandler(error, res);
        }
    }


    async leaveClass(req, res) {
        try {            

            let result = await MemberModel.deleteMember({
                classId: req.params.classId,
                userId: req._user.userId
            })

            res.send({result, message:'success'});
        } catch (error) {
            this._catchErrorHandler(error, res);
        }
    }

    async removeStudent(req, res) {
        try {            

            let result = await MemberModel.deleteMember({
                classId: req.params.classId,
                userId: req.params.stuId
            })

            res.send({result, message:'success'});
        } catch (error) {
            this._catchErrorHandler(error, res);
        }
    }

    async myClasses(req, res) {
        try {            
            let result = await MemberModel.findAllByAggreagate({userId: req._user.userId}, { createdAt: 0, updatedAt: 0});            

            res.send({myClassList: result});
        } catch (error) {
            this._catchErrorHandler(error, res)
        }
    }

    async student(req, res) {
        try {            
            let result = await MemberModel.findAllByParams({classId: req.params.classId, }, { createdAt: 0, updatedAt: 0});            

            res.send({students: result});
        } catch (error) {
            this._catchErrorHandler(error, res)
        }
    }

    async getTask(req, res) {
        try {
            let classId = req.query.classId ? {classId: ObjectId(req.query.classId) } : null            

            let result = await MemberModel.findAllByAggreagate({userId: req._user.userId, ...classId});            

            res.send({myClassList: result});
        } catch (error) {
            this._catchErrorHandler(error, res)
        }
    }

}

module.exports = (app) => new Member(app);