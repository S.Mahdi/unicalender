"use strict"

const router = require('express').Router();
const SurveyModel = require('../models/survey');
const surveyValidation = require('./validators/survey');

class Survey {

    constructor(app) {

        router.post('/Survey/:classId', [surveyValidation.addSurvey, app._Auth.isTeacherLoggedIn.bind(this)], this.addSurvey.bind(this));
        router.get('/Survey', [app._Auth.isUserLoggedIn.bind(this) ], this.getSurvey.bind(this));
        router.delete('/Survey/:id', [app._Auth.isTeacherLoggedIn.bind(this)], this.deleteSurvey.bind(this));

        router.get('/Survey/vote/:voiteId/:index', [app._Auth.isUserLoggedIn.bind(this)], this.addSurveyvoit.bind(this));


        SurveyModel.setModel(app._mongoDB.Survey);
        app.use(router);
        Object.assign(this, app);
    }

    async addSurvey(req, res) {
        try {
            let info = req.body;
            delete info._id;

            info.classId = req.params.classId
            info.createrId = req._user.userId
            let result = await SurveyModel.createSurvey(info);

            res.send({message: 'success'})
        } catch (error) {
            this._catchErrorHandler(error, res)
        }
    }

    async getSurvey(req, res) {
        try {
            let query = {
                createrId: req._user.userId
            }

            if(req.query.classId) {
                query.classId = req.query.classId
            }

            let surveyModel = await SurveyModel.findAllByParams(query);
            delete req.body.id;


            res.send({result: surveyModel || 'notFound'});
        } catch (error) {
            this._catchErrorHandler(error, res)
        }
    }

    async deleteSurvey(req, res) {
        try {
            let result = await SurveyModel.deleteSurvey({_id: req.params.id, createrId: req._user.userId});
            delete req.body.id;


            res.send({status: true, result});
        } catch (error) {
            this._catchErrorHandler(error, res)
        }
    }

    async addSurveyvoit(req, res) {
        try {
            let surveyModel = await SurveyModel.findByParams({_id: req.params.voiteId});
            delete req.body._id;

            if (surveyModel.participant.includes(req._user.userId)) {
                res.status(400).send({message: 'You have already voted'});
                return ;
            }

            surveyModel.participant.push(req._user.userId);
            if(isNaN(surveyModel.result[req.params.index - 1])){
                surveyModel.result[req.params.index - 1] = 0;
            }
            surveyModel.result[req.params.index - 1] += 1;

            surveyModel.markModified('result');
            let result = await SurveyModel.updateByModel(surveyModel, req.body);

            res.send({message: 'success'});
        } catch (error) {
            this._catchErrorHandler(error, res)
        }       
    }
}

module.exports = (app) => new Survey(app);