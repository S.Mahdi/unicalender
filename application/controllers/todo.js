"use strict"

const router = require('express').Router();
const UserModel = require('../models/user');
const todoValidation = require('./validators/todo');

class Todo {

    constructor(app) {

        router.post('/Todo', [app._Auth.isUserLoggedIn.bind(this), todoValidation.addTodo], this.addTodo.bind(this));
        router.get('/Todo', [app._Auth.isUserLoggedIn.bind(this) ], this.getTodo.bind(this));
        router.delete('/Todo/:id', [app._Auth.isUserLoggedIn.bind(this)], this.deleteTodo.bind(this));
        router.patch('/Todo/:id', [app._Auth.isUserLoggedIn.bind(this)], this.editTodo.bind(this));

        router.get('/allTodo', [app._Auth.isUserLoggedIn.bind(this)], this.getAllTodo.bind(this));
        router.delete('/allTodo', [app._Auth.isUserLoggedIn.bind(this)], this.deleteAllTodo.bind(this));

        UserModel.setModel(app._mongoDB.User);
        app.use('/user', router);
        Object.assign(this, app);
    }

    async addTodo(req, res) {
        try {
            let userModel = await UserModel.findByParams({_id: req._user.userId});
            delete req.body.id;

            req.body.id = Date.now().toString() + "-" + (Math.floor(Math.random() * (999 - 0 + 1)) + 0);
            userModel.todo.push(req.body);

            let result = await UserModel.updateByModel(userModel);

            res.send({todo: result.todo});
        } catch (error) {
            this._catchErrorHandler(error, res)
        }
    }

    async getTodo(req, res) {
        try {
            let userModel = await UserModel.findByParams({_id: req._user.userId});
            delete req.body.id;

            let result = userModel.todo.find(element => element.id === req.query.id )

            res.send({result: result || 'notFound'});
        } catch (error) {
            this._catchErrorHandler(error, res)
        }
    }

    async deleteTodo(req, res) {
        try {
            let userModel = await UserModel.findByParams({_id: req._user.userId});
            delete req.body.id;

            let beforDeleteLength = userModel.todo.length;
            userModel.todo = userModel.todo.filter(element => element.id !== req.params.id)

            userModel.markModified('todo')
            let result = await UserModel.updateByModel(userModel);
            let afterDeleteLength = result.todo.length;

            let deleted = beforDeleteLength - afterDeleteLength
            res.send({todo: result.todo, deletedObjectNum: deleted});
        } catch (error) {
            this._catchErrorHandler(error, res)
        }
    }

    async editTodo(req, res) {
        try {
            let userModel = await UserModel.findByParams({_id: req._user.userId});

            let changedNum = 0;
            let newTodoList = userModel.todo.map(element => {
                if(element.id === req.params.id) {
                    delete req.body.id;
                    changedNum = 1;
                    return Object.assign(element, req.body)
                }
                return element
            })
            
            userModel.markModified('todo')
            let result = await UserModel.updateByModel(userModel, {todo: newTodoList});

            res.send({result: result.todo, changedObjectNum: changedNum});
        } catch (error) {
            this._catchErrorHandler(error, res)
        }
    }

    async getAllTodo(req, res) {
        try {
            let projection = {
                todo: 1,
                _id: 0
            }
            let result = await UserModel.findByParams({_id: req._user.userId}, projection);            

            res.send({userInfo: result});
        } catch (error) {
            this._catchErrorHandler(error, res)
        }
    }

    async deleteAllTodo(req, res) {
        try {
            let userModel = await UserModel.findByParams({_id: req._user.userId});

            let result = await UserModel.updateByModel(userModel, {todo: []});

            res.send({message: 'success'});
        } catch (error) {
            this._catchErrorHandler(error, res)
        }
    }

}

module.exports = (app) => new Todo(app);