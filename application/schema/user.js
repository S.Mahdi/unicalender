"use strict";
const mongoose = require('mongoose');
const validators = require('../utils/validators');
const CONFIG = require('../config/' + process.env.NODE_ENV + '.js');

let usersSchema = mongoose.Schema({
    username: {
        required: true,
        type: String
    },
    photoUrl: String,
    password: {
        required: true,
        type: String
    },
    email: {
        type: String,
        require: true,        
        unique: true
    },
    type: {
      type: String,
      default: CONFIG.userTypes.User
    },
    privateTask: {
      type: Array,
      default: []
    },
    todo: {
      type: Array,
      default: []
    },
    verifyCode: {
      type: String
    },
    userVerified: {
      type: Boolean,
      default: false
    }
}, {
    versionKey: false,
    timestamps: true
});

module.exports = (mongoose) => {
    return mongoose.model('Users', usersSchema);
};