"use strict";
const mongoose = require('mongoose');

let lettersSchema = mongoose.Schema({
    classId:{
        type: mongoose.Schema.ObjectId,
        require: true,
    },    
    title: {
        type: String,
        require: true
    },
    content: {
        type: String,
        require: true
    },
    userId: {
        type: String,
        default: null,
    },
    username: String
}, {
    versionKey: false,
    timestamps: true
});

module.exports = (mongoose) => {
    return mongoose.model('letters', lettersSchema);
};