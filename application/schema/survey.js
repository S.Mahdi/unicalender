"use strict";
const mongoose = require('mongoose');

let SurveysSchema = mongoose.Schema({
    title: {
        type: String,
        require: true,
    },
    description: {
        type: String,
    },
    classId:{
        type: mongoose.Schema.ObjectId,
        require: true,
    },
    createrId: {
        type: String,
    },
    options: {
        type: Array,
        require: true,
        default: []
    },
    result: {
        type: Array,
        default: []
    },
    participant: {
        type: Array,
        default: []
    },
    active: {
        type: Boolean,
        default: true
    }
}, {
    versionKey: false,
    timestamps: true
})


module.exports = (mongoose) => {
    return mongoose.model('surveys', SurveysSchema);
};