"use strict";
const mongoose = require('mongoose');
const validators = require('../utils/validators');
const CONFIG = require('../config/' + process.env.NODE_ENV + '.js');

let membersSchema = mongoose.Schema({
    classId:{
        type: mongoose.Schema.ObjectId,
        require: true,
    },
    userId: {
        type: String,
        required: true,
    },
    // teacherId: {
    //     type: String,
    //     required: true,
    // },
    username: String,
}, {
    versionKey: false,
    timestamps: true
});

module.exports = (mongoose) => {
    return mongoose.model('members', membersSchema);
};