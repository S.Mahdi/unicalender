"use strict";
const mongoose = require('mongoose');

let classesSchema = mongoose.Schema({
    createrId: {
        required: true,
        type: mongoose.Schema.ObjectId
    },
    className:{
        type: String,
        require: true
    },
    classEmail: {
        required: true,
        type: String
    },
    photoUrl: {
        type : String,
    },
    task: {
        type: Array,
        default: []
    },
    accessLevel: {
        type: Boolean,
        default: false
    },
    description: String,
}, {
    versionKey: false,
    timestamps: true
});

module.exports = (mongoose) => {
    return mongoose.model('classes', classesSchema);
};