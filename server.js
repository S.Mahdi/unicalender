"use strict"

require('dotenv').config();

process.env.NODE_ENV = process.env.NODE_ENV || 'development';

const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors')
const expressValidator  = require('express-validator');
const errorhandler = require('errorhandler');
const helmet = require('helmet');
const CONFIG = require('./application/config/' + process.env.NODE_ENV + '.js');



let isProduction = process.env.NODE_ENV === 'production';

let app = express();
app._CONFIG = CONFIG;


/**
 * setting middlewares on express server
 */
app.use(helmet());
app.use(cors(function (req, callBack) {
    let options = {
        'optionsSuccessStatus': 200,
        'allowedHeaders': ['Content-Type', 'Authorization'],
        'exposedHeaders': ['Content-Type', 'Authorization']
    };
    callBack(null, options);
}));
app.use(require('morgan')('dev'));
app.use(bodyParser.urlencoded({ limit: '10mb',extended: false }));
app.use(bodyParser.json({limit: '10mb', extended: true}));
app.use(expressValidator({
    customValidators: require('./application/utils/express-custom-validator')()
}));

if (!isProduction) {
    app.use(errorhandler());
}

app.use((req, res, next) => {
    req._errorHandler = require('./application/utils/express-validator-error-handler');
    next();
});


/**
 * mapping body to params for get method
 */
app.use((req, res, next) => {
    if (req.method === 'GET') {
        Object.keys(req.query).forEach((key) => {
            req.params[key] = req.query[key];
        });
    }
    next();
});



/**
 * load main content
 */

require('./application/load')(app);


/**
 * add express listener
 */

let server = app.listen( process.env.PORT || CONFIG.port || 3200 , function(){
    console.log('Listening on port ' + server.address().port);
});